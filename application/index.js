// Import modules
// External modules
const express = require("express");
const app = express();

// Local modules
const tu = require("./tile/updater");
const tl = require("./tile/loader");
const dv = require("./data/viewer");
const da = require("./data/adder");
const sg = require("./style/getter");

// Port
const PORT = process.env.PORT || 8081;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://127.0.0.1");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});


// Get the modifications to update the map (load or cache some tiles)
app.get("/tile/updater", tu.updater);

// Load a tile
app.get("/tile/loader", tl.loader);

// View the data (administrator)
app.get("/data/viewer", dv.viewer);

// Add some data (administrator)
app.post("/data/adder", da.adder);

// Get the style
app.get("/style/getter", sg.getter);

// Listen to the App Engine-specified port, or 8081 otherwise
app.listen(PORT, (err) => {
  console.log("Server listening on port " + PORT);
})
