// Global variables
const config = {
  database: "threedefense",
  host:     "localhost",
  port:     "5432"
}

/**
 * Get the configuration for an unknown visitor
 *
 * @return {Object} PG configuration with the database name, its host,
 *  its port, the user name and its password
 */
function user() {
  config.user = "user";
  config.password = "user";
  return config;
}

// Database authenticator module
exports.user = user;
