var tiles = [];

/**
 * Add a tile to the tile list
 *
 * @param tid {Integer} Tile ID to add
 * @return    {Boolean} TRUE -> tile added | FALSE -> no tile to add
 */
function contains(tid) {
  return tiles.includes(tid);
}

/**
 * Add a tile to the tile list
 *
 * @param tid {Integer} Tile ID to add
 * @return    {Boolean} TRUE -> tile added | FALSE -> no tile to add
 */
function addTile(tid) {
  if (! tiles.includes(tid)) {
    tiles.push(tid);
    return true;
  } else {
    return false;
  }
}

/**
 * Remove a tile from the tile list
 *
 * @param tid {Integer} Tile ID to remove
 * @return    {Boolean} TRUE -> tile removed | FALSE -> no tile to remove
 */
function removeTile(tid) {
  var index = tiles.indexOf(tid);

  if (index > -1) {
    tiles.splice(index, 1);
    return true;
  } else {
    return false;
  }
}

// Tile manager module
exports.tileAdder = addTile;
exports.tileRemover = removeTile;
exports.contains = contains;
