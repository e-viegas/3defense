// Import modules and global variables
const url = require("url");
const pg  = require("pg");
const db  = require("../db/authenticator");
const config = db.user();

/**
 * Get the style from the database
 */
function getter(request, result) {
  var client;
  const pool = new pg.Pool(config);

  pool.connect((err, client, done) => {
    if (client === undefined){
      result.status(500).send(err);
      return;
    }

    client
      .query("SELECT * FROM citygml.style")

      .then(res => {
        client.release(true);
        pool.end();
        result.status(200).send(JSON.stringify(res.rows));
      })

      .catch(err => {result.status(500).send(error)});
  });
}

// Style getter module
exports.getter = getter;
