-- Database: 3defense
-- Template: POSTGIS
CREATE DATABASE threedefense WITH
    OWNER = postgres
	TEMPLATE = 'postgis_25_sample'
    ENCODING = 'UTF8'
    LC_COLLATE = 'French_France.1252'
    LC_CTYPE = 'French_France.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- New schema for the cityGML data
CREATE SCHEMA citygml AUTHORIZATION postgres;


-- ============ TABLES ============
-- A tile is spatial areas, which could be loaded according to the map extent.
CREATE TABLE citygml.tile(
	-- primary key
    id serial PRIMARY KEY NOT NULL UNIQUE,
	
	-- Bound box of this tile
	-- Used to know whether the tile will be loaded or not
    bbox geometry NOT NULL
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- An entity is an object to display on the website.
-- It contains some coordinates, a height for the 3D object and some metadata.
CREATE TABLE citygml.entity(
	-- primary key
    id serial PRIMARY KEY NOT NULL UNIQUE,
	
	-- entity name
    name text NOT NULL,
	
	-- layer name, from which this entity comes
    layer text NOT NULL,
	
	-- Extrusion height, if this entity is a 3D object
	-- For the 2D entity, this field is null
    height numeric
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- An entity is an object to display on the website.
-- It contains some coordinates, a height for the 3D object and some metadata.
CREATE TABLE citygml.spatial_distribution(
	-- foreign key, referencing to an entity
    entity integer NOT NULL,
	CONSTRAINT fke FOREIGN KEY(entity) REFERENCES citygml.entity(id),
	
	-- foreign key, referencing the tile where this entity is located
	tile integer NOT NULL,
	CONSTRAINT fkt FOREIGN KEY(tile) REFERENCES citygml.tile(id),
	
	-- primary key, containing the entity ID and the tile ID.
	-- The couple (entity, tile) must be unique
	CONSTRAINT spatial_pkey PRIMARY KEY(entity, tile)
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- A metadata is a key-value pair to describe an entity.
-- For each entity, the properties must be unique. 
CREATE TABLE citygml.metadata(
	-- foreign key, referencing the entity, described by this metadata.
	-- This field is part of the primary key
    entity integer NOT NULL,
	CONSTRAINT fk FOREIGN KEY(entity) REFERENCES citygml.entity(id),
	
	-- property name used to describe an entity
	-- This field is part of the primary key
    key text NOT NULL,
	
	-- value of this property
    value text NOT NULL,
	
	-- primary key, containing the entity ID and the property name.
	-- The couple (entity, property) must be unique
	CONSTRAINT metadata_pkey PRIMARY KEY(entity, key)
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- A shape is a point collection, as a polygon or a polyline.
CREATE TABLE citygml.shape(
	-- primary key
    id serial PRIMARY KEY NOT NULL UNIQUE,
	
	-- foreign key, referencing the entity using this shape
    entity integer NOT NULL,
	CONSTRAINT fk FOREIGN KEY(entity) REFERENCES citygml.entity(id),
	
	-- flag to link the first and last vertices by an edge
	-- true  -> polygon
	-- false -> polyline
    closed boolean NOT NULL
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- A point is a ponctual entity on the map. It could belongs to a shape.
-- Coordinates system used here : Mercator (EPSG:3857)
CREATE TABLE citygml.point(
	-- primary key
    id serial PRIMARY KEY NOT NULL UNIQUE,
	
	-- foreign key, referencing the shape, for which the point is a vertex.
	-- No foreign key, if the point is isolated
    shape integer,
	CONSTRAINT fks FOREIGN KEY(shape) REFERENCES citygml.shape(id),
	
	-- foreign key, referencing the entity, using this point.
	-- Used for the isolated point
    entity integer,
	CONSTRAINT fke FOREIGN KEY(entity) REFERENCES citygml.entity(id),
	
	-- longitude of this point
    longitude numeric NOT NULL,
	
	-- latitude of this point
    latitude numeric NOT NULL,
	
	-- height of this point.
	-- Ground height by default
    height numeric,
	
	--
    vertex integer
) WITH(OIDS = FALSE) TABLESPACE pg_default;


-- Set the owner for each table
ALTER TABLE citygml.tile OWNER to postgres;
ALTER TABLE citygml.entity OWNER to postgres;
ALTER TABLE citygml.spatial_distribution OWNER to postgres;
ALTER TABLE citygml.metadata OWNER to postgres;
ALTER TABLE citygml.shape OWNER to postgres;
ALTER TABLE citygml.point OWNER to postgres;