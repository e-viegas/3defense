# -*- coding: utf-8 -*-

from scripts.utils import extract
from scripts.model.geometry_set import GeometrySet


class Entity(object):
    """
    Instance for a feature
    """

    def __init__(self, db, feature):
        """
        Create a new feature

        :param db:      Connection to a database
        :type db:       Database Manager

        :param feature: Feature to transform into an entity
        :type feature:  Dictionary with these keys : type, geometry, properties
        """

        self.db = db
        self.id = -1
        self.name = extract(feature["properties"], "gml_name", "")
        self.layer = feature["properties"]["type"]
        self.geometries = GeometrySet(self.db)
        self.properties = feature["properties"]
        self.geometries.build_geometry(feature["geometry"])
        del self.properties["type"]

        self.height = extract(
            feature["properties"],
            "citygml_measured_height",
            None
        )

        # Simple entity
        if "subsets" not in feature:
            return

        for sub in feature["subsets"]:
            self.geometries.build_geometry(sub["geometry"])        # TODO lod ?

    def add(self):
        """
        Add a new entity in the database
        """

        query_add = "INSERT INTO citygml.entity (%s) VALUES (%s) RETURNING id"

        # No height
        if self.height is None:
            self.id = self.db.run(
                query_add % ("name, layer", "%s, %s"),
                (self.name, self.layer)
            )[0][0]

        # With a height
        else:
            self.id = self.db.run(
                query_add % ("name, layer, height", "%s, %s, %s"),
                (self.name, self.layer, self.height)
            )[0][0]

        self.db.commit()

    def add_metadata(self):
        """
        Add the metadata to the database
        """

        for key in self.properties:
            self.db.rerun((
                self.id, key,
                self.properties[key],
                self.properties[key]
            ))

    def link(self, tile):
        """
        Link an entity with a tile

        :param tile: Tile, on where the entity is located
        :type tile:  Tile ID
        """

        self.db.run(
            "INSERT INTO citygml.spatial_distribution (entity, tile)"
            "VALUES (%s, %s)",
            (self.id, tile)
        )
