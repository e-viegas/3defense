# -*- coding: utf-8 -*-

from scripts.utils import same_points


class GeometrySet(object):
    """
    Set of geometries
    """

    def __init__(self, db):
        """
        Initialise the geometry set to an empty set

        :param db: Database connexion
        :type db:  Database manager
        """

        self.lst = []
        self.db = db
        self.id = -1

    def build_geometry(self, geometry):
        """
        Build a geometry as "<lng> <lat> [<h>] ..." according to the coordinates

        :param geometry: Geometry to transform
        :type geometry:  GeoJSON geometry
        """

        # Skip the null geometry
        if geometry is None:
            return

        coordinates = []

        # all points, except the last one if it is a polyline
        for segment in geometry["coordinates"]:
            coordinates.append(segment[0])

        # Polyline or polygon ?
        first_point = geometry["coordinates"][0][0]
        last_point = geometry["coordinates"][-1][0]

        # Polygon
        if same_points(first_point, last_point):
            closed = True

        # Polyline
        else:
            closed = False
            coordinates.append(last_point)

        # Add this new geometry
        self.lst.append({
            "shape": "collection",        # Shape type : point or collection
            "coordinates": coordinates,   # Coordinates
            "closed": closed              # Polyline or polygon (for collection)
        })

    def set_id(self, fid):
        """
        Set the entity ID

        :param fid: Entity ID to set
        :type fid:  Positive integer
        """

        self.id = fid

    def add_geometries(self):
        """
        Insert the geometries into the database
        """

        # Skip the entities without any ID
        if self.id < 0:
            return

        for k in range(len(self.lst)):
            geometry = self.lst[k]

            # Polyline or polygon
            if geometry["shape"] == "collection":
                self.add_collection(geometry)

            # Point
            else:
                self.add_point(geometry, self.id)

    def add_collection(self, geometry):
        """
        Add a point collection

        :param geometry: Geometry to insert
        :type geometry:  Dictionary with these keys : 'closed' and 'coordinates'
        """

        gid = self.db.run(
            "INSERT INTO citygml.shape (entity, closed) VALUES (%s, %s)"
            "RETURNING id", (self.id, geometry["closed"])
        )[0][0]

        # For each point
        for k in range(len(geometry["coordinates"])):
            self.add_vertex(geometry["coordinates"][k], gid, k)

    def add_point(self, point, id_parent):
        """
        Insert an isolated point into the database

        :param point:     Point to insert
        :type point:      Coordinates list (two-item or three-item)

        :param id_parent: Entity ID
        :type id_parent:  Positive integer
        """

        query = "INSERT INTO citygml.point (%s) VALUES (%s)"

        if len(point) < 3:
            # Isolated 2D point
            self.db.run(
                query % ("entity, longitude, latitude", "%s, %s, %s"),
                (id_parent, point[0], point[1])
            )

        else:
            # Isolated 3D point
            self.db.run(
                query % (
                    "entity, longitude, latitude, height",
                    "%s, %s, %s, %s"
                ), (id_parent, point[0], point[1], point[2])
            )

    def add_vertex(self, point, id_parent, index):
        """
        Insert an isolated point into the database

        :param point:     Point to insert
        :type point:      Coordinates list (two-item or three-item)

        :param id_parent: Entity ID
        :type id_parent:  Positive integer

        :param index:
        :type index:
        """

        query = "INSERT INTO citygml.point (%s) VALUES (%s)"

        if len(point) < 3:
            # 2D Point in a collection
            self.db.run(
                query % (
                    "shape, vertex, longitude, latitude",
                    "%s, %s, %s, %s"
                ), (id_parent, index, point[0], point[1])
            )

        else:
            # 3D Point in a collection
            self.db.run(
                query % (
                    "shape, vertex, longitude, latitude, height",
                    "%s, %s, %s, %s, %s"
                ), (id_parent, index, point[0], point[1], point[2])
            )
