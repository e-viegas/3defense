# -*- coding: utf-8 -*-

from scripts.model.entity import Entity
from scripts.utils import extract


class Tile(object):
    """
    Instance for a tile
    """

    def __init__(self, features, db, lower, upper):
        """
        Create a new tile

        :param features: Feature list
        :type features:  GeoJSON format

        :param db:       Connection to a database
        :type db:        Database Manager

        :param lower:    Geographic coordinates for the lower envelope corner
        :type lower:     List as [<x>, <y>] ; Reference System : EPSG:4326

        :param upper:    Geographic coordinates for the upper envelope corner
        :type upper:     List as [<x>, <y>] ; Reference System : EPSG:4326
        """

        self.features = features
        self.lower = lower
        self.upper = upper
        self.db = db
        self.entities = []
        self.id = -1

    def wkt(self):
        """
        Get the WKT string, describing the bound box

        :return: Envelope around the tile, as a polygon
        :rtype:  WKT string
        """

        return "POLYGON((%s, %s, %s, %s, %s))" % (
            "%s %s" % (self.lower[0], self.lower[1]),
            "%s %s" % (self.lower[0], self.upper[1]),
            "%s %s" % (self.upper[0], self.upper[1]),
            "%s %s" % (self.upper[0], self.lower[1]),
            "%s %s" % (self.lower[0], self.lower[1])
        )

    def add(self):
        """
        Add a tile in the database by a SQL query
        """

        self.id = self.db.run(
            "INSERT INTO citygml.tile (bbox) VALUES (ST_GeomFromText(%s))"
            "RETURNING id", variables=(self.wkt(),)
        )[0][0]
        self.db.commit()

    def build_features(self):
        """
        Build the entities and save the list in a attribute
        """

        # Dictionary with all features, ID by ID
        temp = self.merge_data_set()
        removal = []

        # Create a list of "sub-entities" according to the parent ID
        for fid in temp:
            parent = temp[fid][0]

            # Add to a sub-entities list
            if parent in temp:
                sub = temp[parent][1].get("subsets", [])
                sub.append(temp[fid][1])
                temp[parent][1]["subsets"] = sub
                removal.append(fid)

        # Remove the former features
        for fid in removal:
            del temp[fid]

        # Build an entity for each feature
        for fid in temp:
            self.entities.append(Entity(self.db, temp[fid][1]))

    def merge_data_set(self):
        """
        Merge the data sets into a feature list

        :return: All features
        :rtype:  Dictionary as {<id>: (<parent id>, <feature>}
        """

        results = {}

        # Browse each data set
        for data_set in self.features:
            layer = data_set["name"]

            # Skip the city model
            if layer == "CityModel":
                continue

            # For each feature in this data set
            for feature in data_set.get("features", []):
                fid = extract(feature["properties"], "gml_id", "")
                parent = extract(feature["properties"], "gml_parent_id", "")
                feature["properties"]["type"] = layer
                results[fid] = (parent, feature)

        return results

    def browse(self):
        """
        Browse the features to add them in the database
        """

        # Build the feature before browsing them
        self.build_features()

        # Browse the entity list and add the entities in the database
        for entity in self.entities:
            entity.add()
            entity.geometries.add_geometries()
            entity.add_metadata()
            entity.link(self.id)
