# -*- coding: utf-8 -*-

import psycopg2 as pypg


class DatabaseManager(object):
    """
    Manager for PostGreSQL database
    """

    def __init__(self, parameters):
        """
        Create a new database manager and connect to a database

        :param parameters: List of parameters to connect a database
        :type parameters:  String as "dbname=<name> user=<user>"
        """

        # Build the parameter string
        settings = []

        for param in parameters:
            settings.append("%s=%s" % (param, parameters[param]))

        self.parameters = " ".join(settings)
        self.connected = True
        self.conn = pypg.connect(self.parameters)
        self.cur = self.conn.cursor()
        self.query = None

    def run(self, query, variables=()):
        """
        Run a SQL query, which could be generic, on a connected database

        :param query:     Query to run on the database
        :type query:      SQL query

        :param variables: Some variables if the query is generic
        :type variables:  Sequence (list or tuple) | by default: ()

        :return:          Query results
        :rtype:           Tuple list, a tuple is matched with a database entry
        """

        if self.connected:
            self.cur.execute(query, variables)

            try:
                # Fetch some results
                return self.cur.fetchall()

            except pypg.ProgrammingError:
                # Fetch no result
                return []

        else:
            # Disconnected database
            return []

    def save(self, query):
        """
        Save a query to run it with different variables

        :param query: Query to save
        :type query:  SQL query
        """

        self.query = query

    def rerun(self, variables=()):
        """
        Run the last SQL query, saved in the database manager

        :param variables: Some variables if the query is generic
        :type variables:  Sequence (list or tuple) | by default: ()

        :return:          Query results
        :rtype:           Tuple list, a tuple is matched with a database entry
        """

        self.run(self.query, variables)

    def commit(self):
        """
        Commit the modifications
        """

        if self.connected:
            self.conn.commit()

    def disconnect(self):
        """
        Disconnect the database, if it is connected
        """

        if self.connected:
            self.cur.close()
            self.conn.close()
            self.connected = False
