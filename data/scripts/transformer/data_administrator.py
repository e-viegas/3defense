# -*- coding: utf-8 -*-

import sys
from scripts.transformer.citygml_reader import CityGMLReader


db_settings = {
    "dbname": "threedefense",
    "host": "localhost",
    "port": "5432"
}


def authenticate(user, password):
    """
    Authenticate an user

    :param user:     User name
    :type user:      String

    :param password: User password
    :type password:  String

    :return:         Database settings
    :rtype:          Dictionary
    """

    settings = db_settings.copy()
    settings["user"] = user
    settings["password"] = password
    return settings


def run(argv):
    """
    Run a command line by the data administrator and flush the standard output

    :param argv: Arguments for his command line
    :type argv:  String list
    """

    if len(argv) < 2:
        return

    script = argv[1]
    options = argv[2:]

    if script == "provide":
        provide(*options)

    sys.stdout.flush()


def provide(*args):
    """
    Provide data to the database.
    SYNTAX : provide(user, password, fme, dir, ...)
        - <user>     User name
        - <password> User password
        - <fme>      FME workspace used to convert from CityGML to GeoJSON
        - <dir>      Directories, where there are the CityGML files to read

    :param args: Arguments for this command line
    :type args:  Argument list
    """

    db = authenticate(args[0], args[1])
    converter = args[2]
    files = args[3]
    reader = CityGMLReader(converter, files, db)
    tls = reader.run()

    for tl in tls:
        tl.add()
        tl.browse()

    reader.close()


# ========== MAIN ==========
run(sys.argv)

# Run for the tests
# <py> provide postgres postgres converter.fmw
# C:\\Users\\eviegas\\Documents\\3defense-v.2\\data
