# -*- coding: utf-8 -*-


def extract(dic, key, default_val):
    """
    Get a value from dictionary and remove it.

    :param dic:         Dictionary, where the value is located
    :type dic:          Dictionary

    :param key:         Key matched with the value searched
    :type key:          Anything hashable

    :param default_val: Default value if the key does not exist
    :type default_val:  Anything

    :return:            Value matched with the key or the default value
    :rtype:             Anything
    """

    if key in dic:
        value = dic[key]
        del dic[key]
        return value
    else:
        return default_val


# TODO Remove the replicas
def same_points(point_a, point_b, epsilon=10e-3):
    """
    Compare two points.

    :param point_a: First point
    :type point_a:  3-item list as [<x>, <y>, <z>]

    :param point_b: Second point
    :type point_b:  3-item list as [<x>, <y>, <z>]

    :param epsilon: Precision of this comparison
    :type epsilon:  Float, positive as possible

    :return:        TRUE -> same points | FALSE -> different points
    :rtype:         Boolean
    """

    sqr_dist = (point_a[0] - point_b[0]) ** 2\
        + (point_a[1] - point_b[1]) ** 2\
        + (point_a[2] - point_b[2]) ** 2
    return sqr_dist <= epsilon ** 2


def get_geometries(db, fid):
    """
    Get geometries in the database for an entity

    :param db:  Database connexion
    :type db:   Database manager

    :param fid: Entity id
    :type fid:  Positive integer

    :return:    Geometries
    :rtype:     Geometry list
    """

    # Points
    points = db.run(
        "SELECT longitude, latitude, height FROM citygml.point "
        "WHERE entity = %s", (fid,)
    )

    # Polygons
    shapes = db.run(
        "SELECT id, closed FROM citygml.shape WHERE entity = %s", (fid,)
    )

    # Fill the list to return
    geometries = []
    get_points(geometries, points)
    get_shapes(db, geometries, shapes)
    return geometries


def get_points(geometries, points):
    """
    Get points in the database for an entity and add them in a list

    :param geometries: Set where the points will be added
    :type geometries:  Geometry list

    :param points:     SQL results
    :type points:      Tuple list
    """

    for pt in points:
        # Tuple -> list
        coord = list(pt)

        # Remove the z-coordinate for the 2D points
        if coord[2] is None:
            coord.pop()

        # Add the geometry
        geometries.append({
            "shape": "point", "coordinates": coord, "closed": True
        })


def get_shapes(db, geometries, shapes):
    """
    Get points in the database for an entity and add them in a list

    :param db:         Database connexion
    :type db:          Database manager

    :param geometries: Set where the points will be added
    :type geometries:  Geometry list

    :param shapes:     SQL results
    :type shapes:      Tuple list
    """

    for (sid, closed) in shapes:
        # Get the coordinates of the vertices
        coordinates = db.run(
            "SELECT longitude, latitude, height FROM citygml.point "
            "WHERE shape = %s ORDER BY vertex", (sid,)
        )

        for k in range(len(coordinates)):
            # Tuple -> list
            coordinates[k] = list(coordinates[k])

            # Remove the z-coordinate for the 2D points
            if coordinates[k][2] is None:
                coordinates[k].pop()

        # Add the geometry
        geometries.append({
            "shape": "point", "coordinates": coordinates, "closed": closed
        })


def compare_geometry_sets(set_a, set_b):
    """
    Compare two sets of geometries

    :param set_a: First set
    :type set_a:  Dictionary list

    :param set_b: Second set
    :type set_b:  Dictionary list

    :return:      Geometries, both in the two sets
    :rtype:       Geometry list
    """

    equals = []

    for geom_a in set_a:
        for geom_b in set_b:
            if equal_geometry(geom_a, geom_b):
                equals.append(geom_a)

    return equals


def equal_geometry(geom_a, geom_b):
    """
    Compare two geometries

    :param geom_a: First geometry
    :type geom_a:  Dictionary with these

    :param geom_b: Second geometry
    :type geom_b:  Dictionary with these

    :return:       TRUE -> same geometries | FALSE -> else
    :rtype:        Boolean
    """

    if geom_a["shape"] != geom_b["shape"]:
        return False

    elif geom_a["shape"] == "point":
        return same_points(geom_a["coordinates"], geom_b["coordinates"])

    elif geom_a["closed"] != geom_b["closed"]:
        return False

    else:
        return same_points(geom_a["coordinates"], geom_b["coordinates"])
