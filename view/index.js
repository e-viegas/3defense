// Global variables
const url = "http://127.0.0.1:8081/";
const viewer = new Viewer(
  "cesium",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmMDRmNzNkNy1jMGE2LTQ1NDQtO" +
    "TUzNi0yZmNjYjMxZTNmNTQiLCJpZCI6MTIwODEsInNjb3BlcyI6WyJhc3IiLCJnYyJdLCJpY" +
    "XQiOjE1NjAzMjU4NTh9.vsjgIWgofDmMLGHTLgqHPD9l2ZSjAylZdfLp3GpDId0"
);

const tiles = []


/**
 * Update the tiles. This function request a node server.
 */
function update() {
  var extent = viewer.getExtentMap();
  var ajax = new XMLHttpRequest();
  var lst, lst_add_remove;

  var parameters = "tile/updater?lowerlng=" + extent.west
    + "&upperlng=" + extent.east
    + "&lowerlat=" + extent.south
    + "&upperlat=" + extent.north;

  ajax.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      lst = JSON.parse(this.responseText);
      lst_add_remove = addOrRemove(lst);
      loadTiles(lst_add_remove.add);
      removeTiles(lst_add_remove.remove);
    } else if (this.readyState === 4 && this.status === 500) {
      console.log(this.responseText);
    }
  };

  ajax.open("GET", url + parameters, true);
  ajax.send();
}


function addOrRemove(lst) {
  var toAdd = [];
  var toRemove = [];

  for (var k = 0; k < lst.length; k ++) {
    if (! tiles.includes(lst[k])) {
      toAdd.push(lst[k]);
    }
  }

  for (var k = 0; k < tiles.length; k ++) {
    if (! lst.includes(tiles[k])) {
      toRemove.push(tiles[k]);
    }
  }

  return {"add": toAdd, "remove": toRemove};
}


function loadTiles(toLoad) {
  var ajax = new XMLHttpRequest();
  var features;

  ajax.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      features = JSON.parse(this.responseText);

      while(k < features.features.length) {
        if(features.features[k].geometry === "") {
          features.features.splice(k, 1);
        } else {
          k ++;
        }
      };

      console.log(features);

      // Cesium.GeoJsonDataSource.load(JSON.parse(this.responseText))
      //   .then(data => {viewer.cesium.dataSources.add(data)});
      // Cesium.GeoJsonDataSource.load(JSON.parse(this.responseText))
      //   .then(data => {console.log(data);});
    } else if (this.readyState === 4 && this.status === 500) {
      console.log(this.responseText);
    }
  };

  for (var k = 0; k < toLoad.length; k ++) {
    ajax.open("GET", url + "tile/loader?id=" + toLoad[k], true);
    ajax.send();
    tiles.push(toLoad[k]);
  }
}


function removeTiles(toRemove) {
  // console.log(toRemove);
}


update();
getStyle(viewer);

viewer.cesium.camera.changed.addEventListener(function() {
  update();
})
