/**
 * Get the style from the database

 * @param viewer {Object} Viewer instance
 */
function getStyle(viewer) {
  var ajax = new XMLHttpRequest();

  ajax.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      parameters = JSON.parse(this.responseText);
      viewer.setAllStyle(parameters);
      displaySettings(parameters, viewer);
    } else if (this.readyState == 4 && this.status == 500) {
      // TODO
    }
  };

  ajax.open("GET", url + "style/getter", true);
  ajax.send();
}


/**
 * Display the settings as a menu on the website
 *
 * @param settings {Array}  Object for the settings to display
 * @param viewer   {Object} Viewer instance
 */
function displaySettings(settings, viewer) {
  var legend = document.getElementById("legend");
  var div;
  var li;

  // Add each data set
  for (var k = 0; k < settings.length; k ++) {
    layer = settings[k].layer;
    li = document.createElement("li");
    li.classList.add("row");
    div = document.createElement("div");
    div.classList.add("description");
    div.appendChild(checkbox(k, viewer, settings[k].visibility));
    div.appendChild(displayName(layer));
    li.appendChild(div);
    li.appendChild(displayStyle(k, viewer, settings[k]));
    legend.appendChild(li);
  }

  // Limit the width at 42vw
  if (legend.clientWidth > 0.42 * document.body.clientWidth) {
    legend.style.width = "42vw";
    legend.style.overflowX = "scroll";
  }

  // Limit the height at 73vh
  if (legend.clientHeight > 0.73 * document.body.clientHeight) {
    legend.style.height = "73vh";
    legend.style.overflowY = "scroll";
  }
}

/**
 * Get a checkbox for this data set to make it visible or hidden
 *
 * @param dataset_num {String}   Data set index, whose visibility could be
 *    switched by the user
 * @param viewer      {Object}   Viewer instance
 * @param visibility  {Boolean}  Visibility as TRUE = visible and FALSE = hidden
 * @returns           {HTML box} Checkbox to add to the DOM
 */
function checkbox(dataset_num, viewer, visibility) {
  var box = document.createElement("input");
  box.type = "checkbox";
  box.checked = visibility;

  // Event to switch the visibility (visible -> hidden || hidden -> visible)
  box.addEventListener("change", function(ev) {
    changeVisibility(ev, dataset_num, viewer, checkbox.checked);
  });

  return box;
}

/**
 * Display the name of a data set in the menu
 *
 * @param name {String}   Data set name
 * @returns    {HTML box} Text to add to the DOM
 */
function displayName(name) {
  var span = document.createElement("span");
  span.classList.add("dataset");
  span.innerText = fromCamelCase(name);
  return span;
}

/**
 * Convert a text from the camel case to some words. The first letter mubt be
 *   uppercase
 *   Example : CityFurniture -> City furniture
 *
 * @param text {String} Text to convert
 * @returns    {String} Text as a list of words
 */
function fromCamelCase(text) {
  var tmp = text.replace(/(?:^|\.?)([A-Z])/g, function (x, y){
    return "_" + y.toLowerCase()
  }).replace(/^_/, "");
  return tmp.charAt(0).toUpperCase() + tmp.slice(1).replace("_", " ");
}

/**
 * Display the style of a data set in the menu
 *
 * @param dataset Data set index, whose style could be changed by the user
 * @param color   Data set style as a Cesium color instance
 * @returns       HTML div tag to add to the DOM with the adequate background
 */
function displayStyle(dataset, viewer, color) {
  var div = document.createElement("div");
  div.classList.add("style");
  div.style.backgroundColor = getRGBA(color);

  // Event to change the style
  div.addEventListener("click", function(ev) {
    changeStyle(ev, dataset, viewer);
  });

  return div;
}

/**
 * Get the color for CSS style, that is to say : rgba(<r>, <g>, <b>, <a>), where
 *   the values are integers in the range [0, 255]
 *
 * @param color Cesium color instance
 * @returns     String as rgba(<r>, <g>, <b>, <a>)
 */
function getRGBA(color) {
  return "rgba(" + parseInt(255 * color.red) + ", "
    + parseInt(255 * color.green) + ", " + parseInt(255 * color.blue) + ", "
    + parseInt(255 * color.alpha) + ")";
}

/**
 * Change the visibility a layer/data set by checking a box
 *
 * @param ev      Event when the box is checked
 * @param dataset Layer/Data set index to show or hide
 * @param viewer      {Object}   Viewer instance
 * @param checked Current value of the check box (boolean like visibility)
 */
function changeVisibility(ev, dataset, viewer, checked) {
  viewer.style[dataset].visibility = checked;
  // showDataSet(dataset, checked);
}

function changeStyle(ev, dataset, viewer) {
  var color = getColorHex(viewer.style[dataset]);
  var title = fromCamelCase(viewer.style[dataset].layer);
  var visibility = viewer.style[dataset].visibility;

  document.getElementById("wrapper").style.visibility = "visible";
  document.getElementById("dataset").innerText = title;
  document.getElementById("visibility").checked = visibility;
  document.getElementById("picker").farbtastic.setColor(color);
  document.getElementById("cancel").addEventListener("click", cancelStyle);

  document.getElementById("ok").addEventListener("click", ev => {
    submitStyle(ev, dataset, viewer);
  });
}

function getColorHex(color) {
  var red = parseInt(255 * color.red).toString(16);
  var green = parseInt(255 * color.green).toString(16);
  var blue = parseInt(255 * color.blue).toString(16);

  if (red.length === 1) {
    red = "0" + red;
  }

  if (green.length === 1) {
    green = "0" + green;
  }

  if (blue.length === 1) {
    blue = "0" + blue;
  }

  return "#" + red + green + blue;
}

function cancelStyle(ev) {
  document.getElementById("wrapper").style.visibility = "hidden";
}

function submitStyle(ev, dataset, viewer) {
  var visibility = document.getElementById("visibility");
  var entry = document.getElementById("legend").childNodes[dataset];
  var color = document.getElementById("color").style.backgroundColor;
  var colorCode = document.getElementById("picker").farbtastic.color;

  changeVisibility(ev, dataset, viewer, visibility.checked);
  entry.childNodes[0].childNodes[0].checked = visibility.checked;
  entry.childNodes[1].style.backgroundColor = color;
  viewer.style[dataset].red = parseInt(colorCode.substring(1, 3), 16)/255;
  viewer.style[dataset].green = parseInt(colorCode.substring(3, 5), 16)/255;
  viewer.style[dataset].blue = parseInt(colorCode.substring(3, 5), 16)/255;
  document.getElementById("wrapper").style.visibility = "hidden";
}
