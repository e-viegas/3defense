/**
 * Class to manage a Cesium viewer
 * @author Erwan Viegas
 */
class Viewer {
  /**
   * Constructor of the viewer
   * @param container  {String} Name of HTML div where the viewer will be created
   * @param token      {String} Account token
   */
  constructor(container, token){
    Cesium.Ion.defaultAccessToken = token;

    this.cesium = new Cesium.Viewer(container, {
      scene3DOnly: true,
      selectionIndicator: false,
      baseLayerPicker: false
    });

    this.handler = new Cesium.ScreenSpaceEventHandler(this.cesium.scene.canvas);
    this.style = [];
    this.setClock(true, 1);             // Set the clock
    this.provideImagery({assetId: 4});  // Provide imagery
    this.homeCamera(2.2444992, 48.889856, 730, 25, 270, 0);

    // Settings for 3D effects
    this.cesium.scene.globe.depthTestAgainstTerrain = true;
    this.cesium.scene.globe.enableLighting = true;
  }


  /**
   * Set the clock
   *
   * @param animation  {Boolean} Enable the clock animation
   * @param speedup    {Numeric} Speedup of the animated clock
   */
  setClock(animation, speedup) {
    this.cesium.clock.shouldAnimate = animation;
    this.cesium.clock.multiplier = speedup;
  };


  /**
   * Provide imagery for the map
   *
   * @param param    {Object} Parameters for the provider
   */
  provideImagery(param) {
    this.cesium.imageryLayers.addImageryProvider(
      new Cesium.IonImageryProvider(param)
    );
  };


  /**
   * Set the home with the configuration by default
   */
  cameraByDefault() {
    this.cesium.scene.camera.setView({
      duration: 1,
      maximumHeight: 2000,
      pitchAdjustHeight: 2000
    });
  };


  /**
   * Set the home camera
   *
   * @param longitude  {Angular} Longitude of camera position in degrees
   * @param latitude   {Angular} Latitude of camera position in degrees
   * @param altitude   {Angular} Altitude of camera position in meters
   * @param yaw        {Angular} Yaw of camera in degrees (z-axis)
   * @param pitch      {Angular} Pitch of camera in degrees (y-axis)
   * @param roll       {Angular} Roll of camera in degrees (x-axis)
   */
  homeCamera(longitude, latitude, altitude, yaw, pitch, roll) {
    var ori = new Cesium.HeadingPitchRoll.fromDegrees(yaw, pitch, roll);
    var view = getConfig(longitude, latitude, altitude, ori);

    view.endTransform = Cesium.Matrix4.IDENTITY;
    this.cesium.scene.camera.setView(view);

    this.cesium.homeButton.viewModel.command.beforeExecute
        .addEventListener(function(e) {
      e.cancel = true;
      this.cesium.scene.camera.flyTo(view);
    });
  };


  /**
   * Set an event
   *
   * @param type      {String}   Event type
   *  see: https://cesiumjs.org/Cesium/Build/Documentation/ScreenSpaceEventType.html
   * @param feedback  {Function} The instructions to do when the event is caught
   *  Signature required : (feedback: event -> void)
   */
  setEvent(type, feedback) {
    this.handler.setInputAction(
      feedback,
      Cesium.ScreenSpaceEventType[type.toUpperCase()]
    );
  };


  /**
   * zoom to see all entities
   */
  zoomOverEntities() {
    this.cesium.zoomTo(this.cesium.entities);
  };


  /**
   * Set all parameters for the style
   *
   * @param parameters {Array} New parameters
   */
  setAllStyle(parameters){
    this.style = parameters;
  }


  /**
   * Get the current extent map
   */
  getExtentMap() {
    var scratchRectangle = new Cesium.Rectangle();
    var rect = this.cesium.camera.computeViewRectangle(
      this.cesium.scene.globe.ellipsoid,
      scratchRectangle
    );

    return {
      north: rect.north * 180/Math.PI,
      south: rect.south * 180/Math.PI,
      east: rect.east * 180/Math.PI,
      west: rect.west * 180/Math.PI,
    };
  }
}


/**
 * Get a configuration for the home camera
 *
 * @param longitude Longitude of camera position in degrees
 * @param latitude  Latitude of camera position in degrees
 * @param altitude  Altitude of camera position in meters
 * @param ori       Orientation of the camera (HeadingPitchRoll instance)
 * @returns         Object used to initialise the home camera
 */
function getConfig(longitude, latitude, altitude, ori) {
  return {
    // move to this position
    destination: new Cesium.Cartesian3.fromDegrees(
      longitude,
      latitude,
      altitude
    ),

    // Camera orientation
    orientation: {
        heading: ori.heading,
        pitch: ori.pitch,
        roll: ori.roll
    },

    // some settings
    duration: 1,
    maximumHeight: 2000,
    pitchAdjustHeight: 2000
  }
}
